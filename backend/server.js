
var express = require('express');
var bodyParser = require('body-parser');
//var bcrypt = require('bcrypt');
var mysql = require('mysql');

//Inizialize Express App
var app = express();

//Use Middlewares
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));

//Set static Path
app.use('/api', express.static(__dirname));

//Import API Routes
app.use(require('./api/lists_api'));



port = process.env.port || 3000;

app.listen(port, function(){
    console.log("listening to port" + port);
})