
var express = require('express');
var app = express();

var lists = require('../models/lists');

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

app.get('/api', function(req, res, next){
    
    lists.findAll(function(err, rows, fields){
        
        if(err) throw err;
        res.json(rows);
    })
});


module.exports = app;