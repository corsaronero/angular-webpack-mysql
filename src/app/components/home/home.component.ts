
import { Component, Inject, OnInit } from '@angular/core';
import { ListsService } from '../../services/lists.service';

@Component({
	selector: "home",
	templateUrl: "home.component.html",
	providers: [ListsService]
})

export class HomeComponent implements OnInit {
	
	name:string;
	type:string;
	results = [];

	constructor(private listsService: ListsService){
		//this.getLists();
	}
	
	ngOnInit(){
		this.getLists();
	}

	getLists(){
		this.listsService.getLists()
		.subscribe(res => this.results = res);

		console.log("Val:", this.results);
	}
}