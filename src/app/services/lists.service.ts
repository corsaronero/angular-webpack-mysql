

import { Injectable, Inject } from '@angular/core';
import { Http, Headers, RequestOptions,Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

@Injectable()

export class ListsService{

    private headers = new Headers();

    constructor(private http: Http){

        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Access-Control-Allow-Origin', '*');
    }

    getLists(){
        return this.http.get('http://localhost:3000/api', { headers: this.headers })
            .map(res => res.json())
            .catch(this.handleError);
    }

    private handleError (error: Response) {
        console.error(error);
        return Observable.throw(error.json().error || ' error');
    }

}