

<h3>Structure Angular 4 webpack 2 scss + express nodejs mysql</h3>

Git global setup

Create a new repository

git clone https://gitlab.com/corsaronero/angular-webpack-mysql.git
cd angular-webpack-mysql
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Existing folder

cd existing_folder
git init
git remote add origin https://gitlab.com/corsaronero/angular-webpack-mysql.git
git add .
git commit -m "Initial commit"
git push -u origin master

Existing Git repository

cd existing_repo
git remote add origin https://gitlab.com/corsaronero/angular-webpack-mysql.git
git push -u origin --all
git push -u origin --tags